/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.modweb.common.web.view.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import se.ironic.modweb.common.web.support.BaseBean;

/**
 *
 * @author Peter
 */
@Named
@ApplicationScoped
public class MenuBean extends BaseBean implements Serializable {

    List<Menu> menus = Collections.emptyList();

    @Inject
    @Any
    private Instance<MenuGroup> mnuGrpInst;

    @Inject @Any
    private Instance<WebMenuConfig> menuConfInst;
    
    
    public List<Menu> getMenus() {
        if (menus.isEmpty()) {
            assembleMenus();
        }
        return menus;
    }

    private void assembleMenus() {

        MenuAssembler ma = new MenuAssembler();
        for (MenuGroup menuGrp : mnuGrpInst) {
            ma.addGroup(menuGrp);
        }

        for (Iterator<WebMenuConfig> it = menuConfInst.iterator(); it.hasNext();) {
            WebMenuConfig webMenuConfig = it.next();
            for (Map.Entry<String, String> entry : webMenuConfig.getOrderings().entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                ma.addOrdering(key, value);
            }
            for (Map.Entry<String, String> entry : webMenuConfig.getPlacements().entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                ma.addPlacement(key, value);
            }
        }

        menus = ma.assemble();
    }

}
