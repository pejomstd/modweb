/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter
 */
public class SubMenu extends Menu {
    private List<Menu> children;

    public SubMenu(String label, String name) {
        super(label, name);
        this.children = new ArrayList<>();
    }
    
    public SubMenu(String label, String name, List<Menu> children) {
        this(label, name);
        this.children = children;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }
    
    public void addChild(Menu child) {
        this.children.add(child);
    }
    
    
}
