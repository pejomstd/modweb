/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import java.io.Serializable;

/**
 *
 * @author Peter
 */
public abstract class MenuGroup implements Serializable {
    public abstract Menu getRootMenu();
}
