/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import se.ironic.modweb.common.web.support.BaseBean;

/**
 *
 * @author Peter
 */
public class ActionCommand extends BaseBean {
    public String execute(String elExpression) {
        ExpressionFactory expressionFactory = currentContext().getApplication().getExpressionFactory();
        MethodExpression methExpr = expressionFactory.createMethodExpression(currentContext().getELContext(), 
                elExpression, 
                String.class, 
                new Class[]{});
        Object invoke = methExpr.invoke(currentContext().getELContext(), null);
        return (String) invoke;
    }
}
