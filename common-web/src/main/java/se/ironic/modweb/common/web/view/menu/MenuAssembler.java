/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class for assembling a joint menu from different sources. 
 * The sources are "grouped" within a {@link MenuGroup}, witch wrapt a root of a menu tree.
 * 
 * @author Peter
 */
public class MenuAssembler {
    /**
     * TODO clean up a bit. Remove things that may not be nescessary. 
     * Avoid depend on state between runs, that is, a call to assambly() should yield the same
     * if called several times if nothing is added in between.
     */
    public static final String TOP_ORDER = ".ORDER";
    List<MenuGroup> groups = new ArrayList<MenuGroup>();
    private Map<String, String> orders = new HashMap<String, String>();
    private Map<String, String> placements = new HashMap<String, String>();
    private Map<String, MenuGroup> menuGrpMap = new HashMap<String, MenuGroup>();
    private Map<String, Menu> menuMap = new HashMap<String, Menu>();

    public MenuAssembler() {
    }

    /**
     * Adds a group to handle in the assembling of a menu.
     * May be a "top"-menu or a menu for later insertion into the structure of the whole tree.
     * 
     * @param mGroup The group to add to handling
     */
    public void addGroup(MenuGroup mGroup) {
        groups.add(mGroup);
        menuGrpMap.put(mGroup.getRootMenu().getName(), mGroup);
        addGroupMenu(mGroup.getRootMenu());
    }

    /**
     * Adds a meny to assembly.
     * @param menu 
     */
    public void addGroupMenu(Menu menu) {
        menuMap.put(menu.getName(), menu);
    }

    /**
     * Add an ordering to the tree. If ordering for the top-menus, use the constant.
     * {@code TOP_ORDER} as the name.
     * @param name Name of the menu/menu path to order. For top level use the constant. 
     * For other case use the name, for deeper levels separate by ".". Ex: <br/>
     * if you have a structure with top menus "a, b, c", "c" have "c1, c2, c3" and "c2" have "c2a,c2b,c2c", 
     * the name for ordering "c2" would then be "c.c2"
     * 
     * @param ordering comma separated list of names for the menus in which order to have them.
     */
    public void addOrdering(String name, String ordering) {
        orders.put(name, ordering);
    }

    public void addPlacement(String name, String place) {
        placements.put(name, place);
    }

    public List<Menu> assemble() {
        /*
         * Assemble first, sort after
         */
        List<Menu> menues = new ArrayList<Menu>();
        arrangePlacements();

        /* Remaining should be top menues */
        List<String> keys = new ArrayList<String>();
        menuMap.keySet();
        keys.addAll(menuMap.keySet());
        Collections.sort(keys);

        /* Top menu ordering */
        if (orders.containsKey(TOP_ORDER)) {
            String ord = orders.get(TOP_ORDER);
            List<String> asList = Arrays.asList(ord.split(","));

            /* remove all in ordering first to avoid duplicates */
            keys.removeAll(asList);
            keys.addAll(0, asList);
        }

        for (String key : orders.keySet()) {
            if (!key.equals(TOP_ORDER)) {
                int idx = key.indexOf(".") >= 0 ? key.indexOf(".") : key.length();
                Menu rot = menuMap.get(key.substring(0, idx));
                subSort(rot, key, orders.get(key));
            }
        }


        for (String key : keys) {
            menues.add(menuMap.get(key));
        }

        return menues;
    }

    private void arrangePlacements() {
        boolean abort = false;
        while (!placements.isEmpty() && !abort) {
            Set<String> placed = new HashSet<String>();
            int initialSize = placements.size();
            for (Map.Entry<String, String> entry : placements.entrySet()) {
                String key = entry.getKey();
                String placeName = entry.getValue();
                if (placeName.contains(".")) {
                    Menu rot = menuMap.get(placeName.substring(0, placeName.indexOf(".")));
                    Menu toPlace = menuMap.get(key);
                    if (place(rot, toPlace, placeName)) {
                        menuMap.remove(key);
                        placed.add(key);
                    }
                } else {
                    Menu rot = menuMap.get(entry.getValue());
                    Menu toPlace = menuMap.get(key);
                    if (place(rot, toPlace, placeName)) {
                        menuMap.remove(key);
                        placed.add(key);
                    }
                }
            }
            for (String key : placed) {
                placements.remove(key);
            }

            /* If placements did not decrease, probably never gonna happen */
            abort = (initialSize == placements.size());
        }
        System.out.println("");
    }

    private boolean place(Menu mnu, Menu toPlace, String placeName) {
        boolean result = false;
        if (mnu instanceof SubMenu) {
            SubMenu sm = (SubMenu) mnu;
            if (sm.getName().equals(placeName)) {
                sm.addChild(toPlace);
                result = true;
            } else if (!sm.getChildren().isEmpty() && placeName.contains(".")) {
                for (Menu m : sm.getChildren()) {
                    result |= place(m, toPlace, placeName.substring(placeName.indexOf(".") + 1));
                }
            }
        }
        return result;
    }

    private void subSort(Menu mnu, String path, String order) {
        if (mnu instanceof SubMenu) {
            SubMenu sm = (SubMenu) mnu;
            if (sm.getName().equals(path)) {
                sortList(sm.getChildren(), order);
            } else if (!sm.getChildren().isEmpty() && path.contains(".")) {
                for (Menu m : sm.getChildren()) {
                    subSort(m, path.substring(path.indexOf(".") + 1), order);
                }
            }
        }
    }

    private void sortList(List<Menu> list, String order) {
        String[] split = order.split(",");
        Map<String, Order> orderMap = new HashMap<String, Order>();
        /* Start high on all, i.e. list size is enough */
        for (Menu menu : list) {
            orderMap.put(menu.getName(), new Order(menu.getName(), list.size(), menu));
        }
        int idx = 0;
        for (String key : split) {
            if (orderMap.containsKey(key)) {
                orderMap.get(key).ord = idx++;
            }
        }

        List<Order> oList = new ArrayList<Order>();
        oList.addAll(orderMap.values());
        Collections.sort(oList);

        list.removeAll(list);
        for (Order o : oList) {
            list.add(o.menu);
        }

    }

    class Order implements Comparable<Order> {

        String name;
        Integer ord;
        Menu menu;

        public Order(String name, int ord, Menu mnu) {
            this.name = name;
            this.ord = ord;
            this.menu = mnu;
        }

        public int compareTo(Order t) {
            if (this.ord.compareTo(t.ord) != 0) {
                return this.ord.compareTo(t.ord);
            }
            return this.name.compareTo(t.name);
        }
    }
}
