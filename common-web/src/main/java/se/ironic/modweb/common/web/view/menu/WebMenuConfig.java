/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import java.util.Map;

/**
 *
 * @author Peter
 */
public abstract class WebMenuConfig {
    /**
     * Gets configured orderings.
     * @return key/value pairs for ordering
     */
    public abstract Map<String, String> getOrderings();
    /**
     * Gets configured placements.
     * @return key/value pairs for placements
     */
    public abstract Map<String, String> getPlacements();

}
