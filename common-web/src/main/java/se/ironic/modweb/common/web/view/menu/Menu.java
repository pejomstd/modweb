/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

import java.io.Serializable;

/**
 *
 * @author Peter
 */
public abstract class Menu implements Serializable {
    private String label;
    private String name;

    public Menu(String label, String name) {
        this.label = label;
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static class Builder {
        
        protected Menu menu;
        
        public static Builder item(String label, String name) {
            Builder b = new Builder();
            b.menu = new MenuItem(label, name);
            return b;
        }
        
        public static Builder submenu(String label, String name) {
            Builder b = new Builder();
            b.menu = new SubMenu(label, name);
            return b;
        }
        
        public Builder addChild(Menu child) {
            if(menu instanceof SubMenu) {
                SubMenu sm = (SubMenu)menu;
                sm.getChildren().add(child);
            }
            return this;
        }
        
        public Builder withExpression(String expression) { 
            if(menu instanceof MenuItem) {
                MenuItem mi = (MenuItem)menu;
                mi.setExpression(expression);
            }
            return this;
        }
        
        public Menu build() {
            return menu;
        }
    }
}
