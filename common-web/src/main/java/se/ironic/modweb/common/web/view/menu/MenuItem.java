/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.view.menu;

/**
 *
 * @author Peter
 */
public class MenuItem extends Menu {

    private String expression;
    
    public MenuItem(String label, String name) {
        super(label, name);
    }

    public MenuItem(String label, String name, String expression) {
        this(label, name);
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
     
    public String action() {
        return new ActionCommand().execute(expression);
    }
    
}
