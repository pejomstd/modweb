/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.web.support;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Peter
 */
public abstract class BaseBean {
    public FacesContext currentContext() {
        return FacesContext.getCurrentInstance();
    }
    
    public ExternalContext getExternalContext() {
        return currentContext().getExternalContext();
    }
}
