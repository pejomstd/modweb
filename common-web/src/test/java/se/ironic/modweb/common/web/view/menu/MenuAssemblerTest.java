/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.modweb.common.web.view.menu;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Peter
 */
public class MenuAssemblerTest {

    public MenuAssemblerTest() {
    }

    @Test
    public void testAddGroup() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        assertEquals(1, a.groups.size());
    }

    @Test
    public void testAssemble1NoRules() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest").build();
                return root;
            }
        });
        assertEquals(2, a.groups.size());
        assertEquals(2, a.assemble().size());
    }

    @Test
    public void testAssembleOrdering1() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest").build();
                return root;
            }
        });

        assertEquals(2, a.groups.size());
        assertEquals(2, a.assemble().size());
        /* Order before assemble should be the added order */
        List<Menu> assemble = a.assemble();
        assertEquals("atest", assemble.get(0).getName());
        assertEquals("btest", assemble.get(1).getName());

        a.addOrdering(MenuAssembler.TOP_ORDER, "btest,atest");
        assemble = a.assemble();
        assertEquals("btest", assemble.get(0).getName());
        assertEquals("atest", assemble.get(1).getName());

    }

    @Test
    public void testAssembleOrdering2() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest")
                        .addChild(Menu.Builder.item("A", "a").build())
                        .addChild(Menu.Builder.item("B", "b").build())
                        .addChild(Menu.Builder.item("C", "c").build())
                        .build();
                return root;
            }
        });

        assertEquals(2, a.groups.size());
        assertEquals(2, a.assemble().size());
        /* Order before assemble should be the added order */
        List<Menu> assemble = a.assemble();
        assertEquals("atest", assemble.get(0).getName());
        assertEquals("btest", assemble.get(1).getName());

        a.addOrdering(MenuAssembler.TOP_ORDER, "btest,atest");
        a.addOrdering("btest", "c,b");
        assemble = a.assemble();
        assertEquals("btest", assemble.get(0).getName());
        assertEquals("atest", assemble.get(1).getName());
        SubMenu btest = (SubMenu) assemble.get(0);
        assertEquals(3, btest.getChildren().size());
        assertEquals("c", btest.getChildren().get(0).getName());
        assertEquals("b", btest.getChildren().get(1).getName());
        assertEquals("a", btest.getChildren().get(2).getName());

    }

    @Test
    public void testAssemblePlacingMenu1() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest")
                        .addChild(Menu.Builder.item("Item 1", "i1").build())
                        .build();
                return root;
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "ctest").build();
                return root;
            }
        });

        a.addPlacement("ctest", "btest");

        List<Menu> assemble = a.assemble();
        assertEquals(2, assemble.size());
        assertEquals("atest", assemble.get(0).getName());
        assertEquals("btest", assemble.get(1).getName());

        SubMenu sm = (SubMenu) assemble.get(1);
        assertEquals(2, sm.getChildren().size());
    }

    @Test
    public void testAssemblePlacingMenu2() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest")
                        .addChild(Menu.Builder.item("Item 1", "i1").build())
                        .addChild(Menu.Builder.submenu("SM 1", "sm1")
                                .addChild(Menu.Builder.item("Item 2:1", "i2-1").build())
                                .build())
                        .build();
                return root;
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "ctest").build();
                return root;
            }
        });

        a.addPlacement("ctest", "btest.sm1");

        List<Menu> assemble = a.assemble();
        assertEquals(2, assemble.size());
        assertEquals("atest", assemble.get(0).getName());
        assertEquals("btest", assemble.get(1).getName());

        SubMenu sm = (SubMenu) assemble.get(1);
        assertEquals(2, sm.getChildren().size());
        SubMenu get = (SubMenu) sm.getChildren().get(1);
        assertEquals("ctest", get.getChildren().get(1).getName());
    }

    @Test
    public void testAssemblePlacingMenuAndSorting() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "btest")
                        .addChild(Menu.Builder.item("Item 1", "i1").build())
                        .addChild(Menu.Builder.submenu("SM 1", "sm1")
                                .addChild(Menu.Builder.item("Item 2:1", "i2-1").build())
                                .build())
                        .build();
                return root;
            }
        });
        a.addGroup(new MenuGroup() {
            @Override
            public Menu getRootMenu() {
                Menu root = Menu.Builder.submenu("Test", "ctest").build();
                return root;
            }
        });

        a.addPlacement("ctest", "btest");
        a.addOrdering("btest", "ctest,sm1");

        List<Menu> assemble = a.assemble();
        assertEquals(2, assemble.size());
        assertEquals("atest", assemble.get(0).getName());
        assertEquals("btest", assemble.get(1).getName());

        SubMenu btest = (SubMenu) assemble.get(1);
        assertEquals(3, btest.getChildren().size());

        assertEquals("ctest", btest.getChildren().get(0).getName());
        assertEquals("sm1", btest.getChildren().get(1).getName());
        assertEquals("i1", btest.getChildren().get(2).getName());
    }

    @Test
    public void testAssemblePlacingMenuInAnotherPlaced() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("B", "btest").build();
            }
        });
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("C", "ctest").build();
            }
        });
        a.addPlacement("btest", "atest");
        a.addPlacement("ctest", "atest.btest");
        List<Menu> assemble = a.assemble();

        /*
         * atest/btest/ctest
         */
        assertEquals(1, assemble.size());
        SubMenu atest = (SubMenu) assemble.iterator().next();
        assertEquals("atest", atest.getName());
        assertEquals(1, atest.getChildren().size());
        SubMenu btest = (SubMenu) atest.getChildren().get(0);
        assertEquals("btest", btest.getName());
        assertEquals(1, btest.getChildren().size());

        SubMenu ctest = (SubMenu) btest.getChildren().get(0);
        assertEquals("ctest", ctest.getName());

    }

    @Test
    public void testAssemblePlacingMenuInAnotherPlacedAndOrdering() {
        MenuAssembler a = new MenuAssembler();
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("A", "atest").build();
            }
        });
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("B", "btest")
                        .addChild(
                                Menu.Builder.item("B1", "bitem1").build()
                        )
                        .addChild(
                                Menu.Builder.item("B2", "bitem2").build()
                        )
                        .addChild(
                                Menu.Builder.item("B3", "bitem3").build()
                        )
                        .build();
            }
        });
        a.addGroup(new MenuGroup() {

            @Override
            public Menu getRootMenu() {
                return Menu.Builder.submenu("C", "ctest").build();
            }
        });
        a.addPlacement("btest", "atest");
        a.addPlacement("ctest", "atest.btest");
        a.addOrdering("atest.btest", "bitem2,ctest");
        List<Menu> assemble = a.assemble();

        /*
         * atest/btest/ctest
         */
        assertEquals(1, assemble.size());
        SubMenu atest = (SubMenu) assemble.iterator().next();
        assertEquals("atest", atest.getName());
        assertEquals(1, atest.getChildren().size());
        SubMenu btest = (SubMenu) atest.getChildren().get(0);
        assertEquals("btest", btest.getName());
        assertEquals(4, btest.getChildren().size());

        assertEquals("bitem2", btest.getChildren().get(0).getName());
        assertEquals("ctest", btest.getChildren().get(1).getName());
        assertEquals("bitem1", btest.getChildren().get(2).getName());
        assertEquals("bitem3", btest.getChildren().get(3).getName());

    }

}
