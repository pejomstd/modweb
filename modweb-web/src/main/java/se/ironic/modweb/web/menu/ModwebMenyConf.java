/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.modweb.web.menu;

import java.util.HashMap;
import java.util.Map;
import se.ironic.modweb.common.web.view.menu.MenuAssembler;
import se.ironic.modweb.common.web.view.menu.WebMenuConfig;

/**
 *
 * @author Peter
 */
public class ModwebMenyConf extends WebMenuConfig {

    @Override
    public Map<String, String> getOrderings() {
        HashMap<String, String> orderings = new HashMap<>();
        orderings.put(MenuAssembler.TOP_ORDER, "admin,todo,projects,help");
        orderings.put("project", "add_project,list_projects,issues");
        return orderings;
    }

    @Override
    public Map<String, String> getPlacements() {
        HashMap<String, String> placements = new HashMap<>();
        placements.put("issues", "project");
        return placements;
    }
}
