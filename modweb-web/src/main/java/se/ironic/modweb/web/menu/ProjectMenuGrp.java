/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.modweb.web.menu;

import se.ironic.modweb.common.web.view.menu.Menu;
import se.ironic.modweb.common.web.view.menu.MenuGroup;

/**
 *
 * @author Peter
 */
public class ProjectMenuGrp extends MenuGroup {

    @Override
    public Menu getRootMenu() {
        Menu root = Menu.Builder.submenu("Project", "project")
                .addChild(
                        Menu.Builder.item("Add project", "add_project").withExpression("/welcome").build()
                )
                .addChild(
                        Menu.Builder.item("List projects", "list_projects").withExpression("/welcome").build()
                )
                .build();
        return root;
    }

}
