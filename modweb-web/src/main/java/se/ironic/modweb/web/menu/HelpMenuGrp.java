/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.web.menu;

import se.ironic.modweb.common.web.view.menu.Menu;
import se.ironic.modweb.common.web.view.menu.MenuGroup;

/**
 *
 * @author Peter
 */
public class HelpMenuGrp extends MenuGroup {

    @Override
    public Menu getRootMenu() {
        Menu root = Menu.Builder.submenu("Help", "help")
                .addChild(
                        Menu.Builder.item("About", "about").withExpression("/welcome").build()
                )
                .build();
        return root;
    }
}
