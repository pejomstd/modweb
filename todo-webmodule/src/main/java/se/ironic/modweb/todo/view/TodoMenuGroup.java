/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.todo.view;

import se.ironic.modweb.common.web.view.menu.Menu;
import se.ironic.modweb.common.web.view.menu.MenuGroup;

/**
 *
 * @author Peter
 */
public class TodoMenuGroup extends MenuGroup {

    @Override
    public Menu getRootMenu() {
        return Menu.Builder.submenu("Todo", "todo")
                .addChild(Menu.Builder.item("View lists", "view_lists").withExpression("/todo/todolists").build())
                .addChild(Menu.Builder.item("Create list", "create_list").withExpression("/todo/create-list").build())
                .build();

    }
    
}
