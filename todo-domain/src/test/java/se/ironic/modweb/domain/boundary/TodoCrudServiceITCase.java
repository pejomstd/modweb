/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.modweb.domain.boundary;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.ejb.embeddable.EJBContainer;
import javax.inject.Inject;
import javax.naming.Context;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import static org.mockito.Mockito.mock;
import se.ironic.modweb.common.annotation.DataAccessor;
import se.ironic.modweb.domain.entity.TodoEntry;
import se.ironic.modweb.domain.entity.TodoList;

/**
 *
 * @author Peter
 */
@ManagedBean
public class TodoCrudServiceITCase {

    @Inject
    @DataAccessor(name = "Todo")
    private TodoCrudService crudService;

    @Resource
    private UserTransaction userTransaction;

    public TodoCrudServiceITCase() {
    }

    @Before
    public void setUp() throws Exception {
        Properties p = new Properties();
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        p.put("jdbc/TodoDB", "new://Resource?type=DataSource");
        p.put("jdbc/TodoDB.JdbcDriver", "org.apache.derby.jdbc.EmbeddedDriver");
        p.put("jdbc/TodoDB.UserName", "todo");
        p.put("jdbc/TodoDB.Password", "todo");
        p.put("jdbc/TodoDB.ConnectionURL", "jdbc:derby:tododb;create=true");
//        p.put(Context.SECURITY_PRINCIPAL, "testUser");
//        p.put(Context.SECURITY_CREDENTIALS, "testPwd");
        EJBContainer.createEJBContainer(p).getContext().bind("inject", this);

    }

    @Test
    public void testCrud() throws Exception {
        try {
            userTransaction.begin();
            List all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(0, all.size());

            String name = "Testlist";
            TodoList list = new TodoList(name);
            /* Test create */
            TodoList create = crudService.create(list);

            all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(1, all.size());

            /* find flushes so after we should have id in "create" */
            assertNotNull(create.getId());
            assertNotNull(create.getVersion());

            assertEquals(name, create.getName());
            name = "Changed";
            create.setName(name);
            /* Test update */
            TodoList update = crudService.update(create);

            /* Test find (and that update worked) */
            TodoList find = crudService.find(TodoList.class, create.getId());
            assertNotNull(find);
            assertEquals(name, find.getName());

            /* Test delete */
            crudService.delete(find);

            all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(0, all.size());

        } catch (Exception ex) {
            Logger.getLogger(TodoCrudServiceITCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("Should not get Exception");
        } finally {
            userTransaction.rollback();
        }
    }

    @Test(expected = OptimisticLockException.class)
    public void testFailVersion() throws Exception {
        try {
            userTransaction.begin();
            TodoList todoList = new TodoList("Testlist");
            TodoList create = crudService.create(todoList);

            crudService.flush();
            crudService.getEntityManager().detach(todoList);
            List all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(1, all.size());
            TodoList find = crudService.find(TodoList.class, create.getId());

            find.setName("Changed");
            crudService.update(find);
            crudService.flush();

            find = crudService.find(TodoList.class, find.getId());
            assertNotSame(todoList.getVersion(), find.getVersion());

            crudService.update(todoList);
            fail("Should have gotten Optimistic lock Ex.");
        } finally {
            userTransaction.rollback();
        }
    }

    @Test
    public void testListWithEntries() throws Exception {
        try {
            userTransaction.begin();
            TodoList todoList = new TodoList("Testlist");

            TodoEntry te1 = creteEntry("#1", "d:#1");
            todoList.getEntries().add(te1);
            TodoList create = crudService.create(todoList);
            crudService.flush();
            List<TodoList> all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(1, all.size());
            assertEquals(1, all.iterator().next().getEntries().size());
            
            Long id = create.getId();
            
            crudService.flush();
            TodoList find = crudService.find(TodoList.class, id);
            TodoEntry te2 = creteEntry("#2", "d:#2");
            find.getEntries().add(te2);
            
            crudService.update(find);
            crudService.flush();
            crudService.getEntityManager().clear();
            
            find = crudService.find(TodoList.class, id);
            Iterator<TodoEntry> iterator = find.getEntries().iterator();
            iterator.next();
            iterator.remove();
            
            crudService.flush();
            crudService.getEntityManager().clear();
            
            List allEntries = crudService.findByNamedQuery(TodoEntry.ALL);
            
            all = crudService.findByNamedQuery(TodoList.ALL);
            assertEquals(1, all.size());
            assertEquals(1, all.iterator().next().getEntries().size());
        } finally {
            userTransaction.rollback();
        }
    }

    private TodoEntry creteEntry(String title, String desc) {
        return new TodoEntry(title, desc, new Date());
    }
}
