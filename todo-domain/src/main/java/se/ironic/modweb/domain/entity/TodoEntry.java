/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.domain.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;

/**
 *
 * @author Peter
 */
@NamedQueries({
    @NamedQuery(name = TodoEntry.ALL, query = "SELECT t FROM TodoEntry t")
})
@Entity
public class TodoEntry extends BaseEntity {
    public static final String ALL = "TodoEntry.findAll";
    private String title;
    private String description;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dueDate;
    private Boolean done = Boolean.FALSE;

    public TodoEntry() {
    }

    public TodoEntry(String title, String description, Date dueDate) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Boolean isDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
    
    @Override
    public boolean equals(Object o) {
        if(o instanceof TodoEntry) {
            TodoEntry other = (TodoEntry)o;
            return this.getId().equals(other.getId());
        }
        return false;
    }
    
}
