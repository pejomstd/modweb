/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.domain.boundary;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.ironic.modweb.common.annotation.DataAccessor;
import se.ironic.modweb.common.support.CrudService;

/**
 * Implementation of CrudService for Todo-domain.
 * @author Peter
 */
@DataAccessor(name = "Todo")
public class TodoCrudService extends CrudService {
    
    @PersistenceContext(unitName = "TodoPU")
    protected EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
}
