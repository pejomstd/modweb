/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.domain.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Peter
 */
@Entity
@NamedQueries({
    @NamedQuery(name = TodoList.ALL, query = "SELECT t FROM TodoList t")
})
public class TodoList extends BaseEntity {
    public static final String ALL = "se.ironic.modweb.domain.entity.TodoList.findAll";
    private String name;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "entry_id")
    private List<TodoEntry> entries = new ArrayList<>();

    public TodoList() {
    }

    public TodoList(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TodoEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<TodoEntry> entries) {
        this.entries = entries;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof TodoList) {
            TodoList other = (TodoList)o;
            return this.getId().equals(other.getId());
        }
        return false;
    }
    
    
    
    
}
