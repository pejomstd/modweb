/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.support;

import javax.enterprise.util.AnnotationLiteral;
import se.ironic.modweb.common.annotation.DataAccessor;

/**
 * Implmentation class for use if programmatic inject is needed for DataAccessor.
 * @author Peter
 */
public class DataAccessorImpl extends AnnotationLiteral<DataAccessor> implements DataAccessor {

    private String name;
    
    public DataAccessorImpl(String name) {
        this.name = name;
    }
    
    @Override
    public String name() {
        return this.name;
    }
    
}
