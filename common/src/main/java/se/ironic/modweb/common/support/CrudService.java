/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package se.ironic.modweb.common.support;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Basic implementation for common methods for a CrudService.
 * @author Peter
 */
public abstract class CrudService {
    /**
     * Provide EntityManger from implementing class for access for generic methods.
     * @return 
     */
    public abstract EntityManager getEntityManager();
    
    public <T> T create(T entity) {
        getEntityManager().persist(entity);
        return entity;
    }
    
    public <T> T find(Class<T> clazz, Object key) {
        return getEntityManager().find(clazz, key);
    }
    
    public <T> T update(T entity) {
        getEntityManager().merge(entity);
        return entity;
    }
    
    public <T> void delete(T entity) {
        getEntityManager().remove(entity);
    }
    
    public List findByNamedQuery(String query) {
        Query nq = getEntityManager().createNamedQuery(query);
        return nq.getResultList();
    }
    
    public void flush() {
        getEntityManager().flush();
    }
    
}
